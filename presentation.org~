#+TITLE: Model assessment and validation in data science
#+SUBTITLE: Why my 85% might be better than your 99% 
#+AUTHOR: Mathieu Fauvel
#+EMAIL: mathieu.fauvel@inrae.fr
#+DATE: [2020-10-06 Tue]

#+LANGUAGE: en
#+OPTIONS: toc:t H:2 tags:nil d:nil

#+PROPERTY: header-args :eval yes

#+LaTeX_CLASS_OPTIONS: [pressentation,10pt,aspectratio=1610,xcolor=table, serif]
#+BEAMER_THEME: Cesbio
#+BEAMER_HEADER: \usefonttheme[onlymath]{serif}


#+LATEX_HEADER: \newcommand{\shorttitle}{Model assessment}
#+LATEX_HEADER: \newcommand{\shortauthor}{M. Fauvel - @MaFauvel}
#+LATEX_HEADER: \institute{CESBIO, Universit{\'e} de Toulouse, CNES/CNRS/INRAe/IRD/UPS, Toulouse, FRANCE}
#+LATEX_HEADER: \titlegraphic{\includegraphics[width=0.2\textwidth]{./figures/logo_cesbio_transp.pdf}~\hfill~\includegraphics[width=0.25\textwidth]{./figures/logo_inrae.pdf}~\hfill~\includegraphics[width=0.25\textwidth]{./figures/logo_ANITIvec.pdf}~\vspace{3cm}}

# #+BEAMER_HEADER: \setbeamercovered{again covered={\opaqueness<1->{25}}}


#+LATEX_HEADER: \usepackage[french]{babel}\usepackage{etex}\usepackage{pifont}\usepackage{booktabs}\usepackage{collcell}
#+LATEX_HEADER: \usepackage{tikz}\usepackage{amsmath, amssymb, wasysym} \usepackage[T1]{fontenc}\usepackage{lmodern}%\usepackage[babel=true,kerning=true]{microtype}
#+LATEX_HEADER: \usepackage{pgfplots,pgfplotstable}\usetikzlibrary{babel,dateplot,mindmap,trees,shapes,arrows.meta,spy,3d,backgrounds,positioning,pgfplots.statistics,calc,fit,overlay-beamer-styles, chains, quotes,shapes.geometric, decorations.markings, shapes.callouts,decorations.pathmorphing}\usepgfplotslibrary{groupplots}\pgfplotsset{compat=newest}\usepackage{minted}
#+LATEX_HEADER: \pgfplotsset{/pgf/number format/assume math mode=true}\usepackage{csquotes}
# \usepackage[T1]{fontenc}\usepackage{textcomp,lmodern}\usepackage[small,T1}{eulervm}
#+LATEX_HEADER: \renewenvironment{description}{\begin{itemize}}{\end{itemize}}\usepackage{smartdiagram}
#+LATEX_HEADER: \usepackage[citestyle=authoryear, bibstyle=verbose, backend=biber]{biblatex}\addbibresource{../../references.bib}
#+LATEX_HEADER: \usepackage[type={CC}, modifier={by-sa}, version={4.0},]{doclicense}

#+LATEX_HEADER: \usepackage{pgfpages}
#+LATEX_HEADER: \setbeameroption{hide notes}

* Introduction
** PhD Infos
Thesis :
- Started in January 2019
- Full-time at CESBIO, Toulouse

Supervisors :
- Director : Mathieu FAUVEL, CESBIO, CNES, INRAE, IRD, UPS
- Co-director : Clément MALLET, Univ. Gustave Eiffel, LaSTIG STRUDEL, IGN, ENSG

** Very large scale learning for remote sensing image time series classification 
*** PhD Objectives
Development of an efficient large scale learning method that can :
- use large amount of features
- deal with noisy measurement and / or noisy labels
- extract relevant information from labelled classes

** Issues to address

Data volume (for example, Sentinel-2):
- 100 billions of pixels
- dozen of Terrabytes of data

Noise in the labels :
- affect classification performance
- label-noise robust learning


* Robust mixture discriminant analysis

** Partition Mixture Classifier (1/2)
The mixture model is given by cite:bouveyron_robust_2009.  We  have  two structures in the data set:
1. An unsupervised structure of \(K\) clusters, associated to the random variables \(S\),
2. A supervised structure of \(C\) classes, associated to the random variable \(Z\).
and we observe \(\big\{\mathbf{x}_\ell,   z_\ell\big\}_{l=1}^n\)  training   samples,  where \(\mathbf{x}_\ell\in\mathbb{R}^d\) are independent realisations of the random vector \(X\) and \(z_\ell\) are the associated labels, independent realisations of \(Z\). The conventional mixture model assumes that
#+BEGIN_EXPORT latex
\begin{eqnarray}
  p(\mathbf{x}) = \sum_{j=1}^Kp(\mathbf{X}=\mathbf{x}|S=j)p(S=j).\label{eq:mm}
\end{eqnarray}
#+END_EXPORT

If we assume that there is a structure classes represent all the data set, we have the following equality:
#+BEGIN_EXPORT latex
\begin{eqnarray}
  \sum_{i=1}^Cp(Z=i|S=j) = 1,\ \forall j\in\{1,\ldots,K\}\label{eq:ck}
\end{eqnarray}
#+END_EXPORT

** Partition Mixture Classifier (2/2)

Plugging ref:eq:ck into ref:eq:mm we obtain
#+BEGIN_EXPORT latex
\begin{eqnarray}
  \label{eq:mmm}
  p(\mathbf{x}) = \sum_{i=1}^C\sum_{j=1}^Kp(Z=i|S=j)p(\mathbf{X}=\mathbf{x}|S=j)p(S=j).
\end{eqnarray}
#+END_EXPORT
The term \(p(Z=i|S=j)\) can be interpreted as the probability that the \(j\)th clusters belongs to the \(c\)th class. For simplicity, it is denoted \(r_{ij}\) in the following.

** Parameter estimation (1/2)

Under the positivity and sum-to-one condition and i.i.d assumption for the samples, the likelihood can be written as 
#+BEGIN_EXPORT latex
\begin{eqnarray*}
  L(\mathbf{X}, \mathbf{z}, \mathbf{r}) &=& \prod_{\ell=1}^n p(\mathbf{X}=\mathbf{x}_\ell, Z=z_\ell) \\
  &=& \prod_{\ell=1}^np(Z=z_\ell |\mathbf{X}=\mathbf{x}_\ell)p(\mathbf{X}=\mathbf{x}_\ell)
\end{eqnarray*}
#+END_EXPORT

We can show that the negative log-likelihood is equal to 
#+BEGIN_EXPORT latex
\begin{eqnarray*}
  l(\mathbf{\mathbf{X}, \mathbf{z}, \mathbf{r}}) & \propto & \sum_{l=1}^n-\ln\Big\{\mathbf{z}_{l}^\top\mathbf{R}^\top\boldsymbol{\psi}_l\Big\} 
\end{eqnarray*}
#+END_EXPORT
where \(\xi=-\sum_{\ell=1}^{n}\ln\big\{p(\mathbf{x}_\ell)\big\}\) does not depend on \(\mathbf{R}\) and \(\boldsymbol{\beta}_\ell\in\mathbb{R}^C\) with \(\beta_{\ell i}=1\) if \(i=z_\ell\) otherwise \(\beta_{\ell i}=0\). 



** Parameter estimation (2/2)

The optimization can be stated as : 
#+BEGIN_EXPORT latex
\begin{eqnarray}
\label{eq:ll}
\begin{array}{rl}
\displaystyle{\min_{\mathbf{r}}}
&\displaystyle{\sum_{l=1}^n -\ln\big(\mathbf{z}_l^\top\mathbf{R}\boldsymbol{\psi}_l\big)}
\\
\text{Constraint to} & \mathbf{R}^\top\mathbf{1}_C = \mathbf{1}_K\\
& \mathbf{R} \succcurlyeq 0.
\end{array}
\end{eqnarray}
#+END_EXPORT

Or equivalently :
#+BEGIN_EXPORT latex
\begin{eqnarray}
\label{eq:ll:c}
\begin{array}{rl}
\displaystyle{\min_{\mathbf{r}}}
&\displaystyle{\sum_{i=1}^C \sum_{\substack{l=1\\ z_l \in i}}^{n_i}  -\ln\big(\mathbf{r}_i^\top \boldsymbol{\psi}_l\big)}
\\
\text{Constraint to} & \mathbf{R}^\top\mathbf{1}_C = \mathbf{1}_K\\
& \mathbf{R} \succcurlyeq 0
\end{array}
\end{eqnarray}
#+END_EXPORT

** Convex proof

+ In order to show that \(f\) is convex, we use composition rules cite:boyd-2004-conve-optim:
  - \(f\) is the summation of \(n\) functions \(h_\ell(\mathbf{R}) = - \ln\big(\boldsymbol{\beta}_\ell^\top\mathbf{R}\boldsymbol{\psi}_\ell\big)\)_{+}
  - \(h_\ell(\mathbf{R})\) is the composition of a three convex functions,
    - Linear operator,
    - Max scalar function,
    - Negative logarithm scalar function,
    therefore it is convex.
  - Hence \(f\) is convex

+ \(g_{1}\) is the indicator function on the set \(\mathbb{R}_+\) and it is convex, see cite:boyd-2004-conve-optim.
+ Same comments applied to \(g_2\).

Hence ref:eq:ll is a convex constraint optimization problem and dedicated solver can be used.

* Convex problem solving with ADMM
** TO DO : continuer
* Simulations
** Ex

#+begin_src python :session :exports results :results file
import matplotlib
import matplotlib.pyplot as plt
fig=plt.figure(figsize=(3,2))
plt.plot([1,3,2]
fname = "images/yolo.pdf"
plt.savefig(fname)
return(fname)
#+end_src

#+RESULTS:
[[file:]]


** 
* Toward a better clustering

** Supervised clustering

** Non-gaussian clustering

* Conclusion
** Conclusion

** References
:PROPERTIES:
:BEAMER_OPT: fragile,allowframebreaks,label=ref
:END:      
\printbibliography[heading=none]

** Licence
:PROPERTIES:
:BEAMER_opt: noframenumbering,
:END:
#+begin_center
\small
\doclicenseLongText

\doclicenseImage
#+end_center
