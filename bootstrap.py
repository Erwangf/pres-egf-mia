import warnings
warnings.filterwarnings('ignore')

import matplotlib.pyplot as plt
import numpy as np
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.neural_network import MLPClassifier as NNC
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.datasets import make_classification
from joblib import Parallel, delayed
import pandas as pd
X,y = make_classification(n_samples=500, n_features=50,
                          n_informative=40, n_redundant=10,
                          n_classes=5, n_clusters_per_class=2)

# Standardize data
sc = MinMaxScaler()
X = sc.fit_transform(X) # Scale data between 0 and 1

classifiers = [SVC(kernel="rbf", gamma=1, C=10),
               RF(n_estimators=500, n_jobs=1),
               NNC(hidden_layer_sizes=(100, 50, 40,),
                   random_state=0, alpha=0.1, max_iter=500)]

def compute_bootstrap(random_state, X, y, classifiers):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, train_size=0.75, test_size=0.25,
        random_state=random_state)
    local_res = []
    for clf in classifiers:
        clf.fit(X_train, y_train)
        local_res.append(clf.score(X_test, y_test))
    return local_res

res =  Parallel(n_jobs=-1)(delayed(compute_bootstrap)(
    rs, X, y, classifiers) for rs in range(50))

df = pd.DataFrame(data=res, columns=["SVC", "RF", "NN"])
df.to_csv("/home/mfauvel/Documents/Recherche/INRA/Rapport/ds-cb/Mathieu_Model_Validation/figures/res_bootstrap.csv", index_label="iterations")
