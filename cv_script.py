import warnings
warnings.filterwarnings('ignore')

import matplotlib.pyplot as plt
import scipy as sp
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold, GridSearchCV, train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import accuracy_score
from sklearn.datasets import make_classification

X,y = make_classification(n_samples=1000, n_features=50,
                          n_informative=40, n_redundant=10,
                          n_classes=5, n_clusters_per_class=2)
# Standardize data
sc = MinMaxScaler()
X = sc.fit_transform(X) # Scale data between 0 and 1
X_train, X_test, y_train, y_test = train_test_split(
    X, y, train_size=0.75, random_state=0)

# Hyperparameters
C = 10.0**sp.arange(-1, 3) # Penality of the optimization problem
gamma = 2.0**sp.arange(-4, 4) # Scale of the RBF kernel
params = dict(kernel=['rbf'], gamma=gamma, C=C)

# Do Cross Validation
grid = GridSearchCV(SVC(),  # Set up the classifier
                    param_grid=params, cv= 5,
                    refit=True, n_jobs=-1) # Do the grid search in parallel
grid.fit(X_train, y_train) # Run the grid search

print(grid.best_score_)
print(grid.best_params_)
# Predict new samples
grid.best_estimator_.score(X_test, y_test)
